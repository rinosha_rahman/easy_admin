import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { StopRoutingModule } from './stop-routing.module';
import { StopComponent } from './stop.component';
import { InputGroupModule, InputTextModule as mkInputTextModule, BoxModule } from 'angular-admin-lte';

@NgModule({
  declarations: [StopComponent],
  imports: [
    CommonModule,
    StopRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    InputGroupModule,
    mkInputTextModule,
    BoxModule
  ]
})
export class StopModule { }
