import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RouteRoutingModule } from './route-routing.module';
import { RouteComponent } from './route.component';

import { InputGroupModule, InputTextModule as mkInputTextModule, BoxModule } from 'angular-admin-lte';

@NgModule({
  declarations: [RouteComponent],
  imports: [
    CommonModule,
    RouteRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    InputGroupModule,
    mkInputTextModule,
    BoxModule
    
  ]
})
export class RouteModule {   }
