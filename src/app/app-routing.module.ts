import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { DriverComponent } from './driver/driver.component';
import { BusComponent } from './bus/bus.component';
import { RouteComponent } from './route/route.component';
import { StopComponent } from './stop/stop.component';

const routes: Routes = [
  {
    path: '', component: LoginComponent, data: {
      customLayout: true //side bar
    }
  },
  { path: 'driver', component: DriverComponent },
  { path: 'bus', component: BusComponent },
  { path: 'route', component: RouteComponent },
  { path: 'stop', component: StopComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
