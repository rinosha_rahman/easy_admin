export const adminLteConf = {
  skin: 'blue',
  
  sidebarLeftMenu: [
    
    
    {label: 'COMPONENTS', separator: true},
    {label: 'Drivers', route: 'driver', iconClasses: 'fa fa-user', },
    {label: 'Buses', route: 'bus', iconClasses: 'fa fa-bus'},
    {label: 'Stops', route: 'stop',iconClasses: 'fa fa-ban', },
    {label: 'Routes', route: 'route', iconClasses: 'fa fa-road'},
    {label: 'Log Out', route: '/', iconClasses: 'fa fa-power-off',},
    
    
  ]
};
