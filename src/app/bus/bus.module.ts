import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BusRoutingModule } from './bus-routing.module';
import { BusComponent } from './bus.component';

import { InputGroupModule, InputTextModule as mkInputTextModule, BoxModule } from 'angular-admin-lte';

@NgModule({
  declarations: [BusComponent],
  imports: [
    CommonModule,
    BusRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    InputGroupModule,
    mkInputTextModule,
    BoxModule
  ]
})
export class BusModule { }
